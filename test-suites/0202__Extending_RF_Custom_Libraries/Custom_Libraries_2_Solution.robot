*** Settings ***
Documentation  Example for generating and using a (java) remote library.
Library  Remote  http://127.0.0.1:8270/  WITH NAME  JavaRemote


*** Test Cases ***
Example
  ${return_value}=  JavaRemote.Print Message  Tadaaa!
  Log  ${return_value}

Exercise 1a
  [Tags]  not_ready
  ${result}=  JavaRemote.join two strings  hello  world
  Should be equal  ${result}  hello|world

Exercise 1b
  [Tags]  not_ready
  ${res}=  JavaRemote.Count Number Of Words  ${RESOURCES}\\sample.txt
  Should Be Equal As Integers  ${res}  10