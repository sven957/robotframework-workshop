# Robot Framework Workshop

## Basics and Syntax

### Test Suite Sections
![Robot Framework Syntax](syntax.png)
* __*** Settings***__
  * import libraries, variable files, resource files and defining metadata and setup

* __*** Variables ***__
  * define test suite variables

* __*** Test Cases ***__
  * create test cases from available keywords

* __*** Keywords ***__
  * create user keywords from lower-level keywords
  * Settings for keywords
    * [Documentation]
    * [Tags]
    * [Arguments] (positional, named-only, free named arguments)
    * [Return]
    * [Teardown]
    * [Timeout]

### Cheat Sheet
* separator: **2 spaces** or pipe 
* ${variable} | scalar variable
* @{list} | list variable entry 1 | list variable entry 2
* &{dictionary} | name=me | ${variable}=robo
* %{environment_var}
* #comment
* empty character: ${EMPTY} 
* space character: ${SPACE} 
* Escape sign: \
