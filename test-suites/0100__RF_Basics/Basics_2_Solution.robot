*** Settings ***
Documentation  Basic examples with file handling and for loop in Robot Framework.
Library  OperatingSystem
Library  String
Library  Collections


*** Variables ***
${filename}  customers.csv
${expected_content}    SEPARATOR=\n
...             Mueller|42|Hamburg
...             Meier|67|Muenchen
...             Wuensch|38|Koeln


*** Test Cases ***
Example: create and move file
   Create File  ${filename}  ${expected_content}
   Move File    ${filename}  ${TEMPDIR}/${filename}

Customer File Existence
  File Should Exist  ${TEMPDIR}/${filename}

File Content Should Be As Expected
  ${content}=  Get File  ${TEMPDIR}/${filename}
  Should Be Equal  ${content}  ${expected_content}
  Set Suite Variable  ${content}  ${content}

Loop through file content
  @{lines}=  Split To Lines  ${content}
  Log Many  @{lines}
  @{rows}=  Create List  @{EMPTY}
  FOR  ${line}  IN  @{lines}
    @{entries}=  Split String  ${line}  |
    Append To List  ${rows}  ${entries}
  END
  Log  ${rows}