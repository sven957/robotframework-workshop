*** Settings ***
Documentation  Basic examples with file handling and for loop in Robot Framework.
Library  OperatingSystem


*** Variables ***
${filename}  customers.csv
${expected_content}    SEPARATOR=\n
...             Mueller|42|Hamburg
...             Meier|67|Muenchen
...             Wuensch|38|Koeln


*** Test Cases ***
Example: create and move file
   Create File  ${filename}  ${expected_content}
   Move File    ${filename}  ${TEMPDIR}/${filename}