*** Settings ***
Documentation  Basic examples with Robot Framework data types, libraries, error handling and logging.
Library  String
Library  Collections
Library  OperatingSystem


*** Variables ***
&{mountain_elevation}=  Brocken=1141  Zugspitze=2962
@{list}  1  2  3  4  5  6  7  42
${seven_number}  ${7}
${seven_string}  7
${expected_status}  false


*** Test Cases ***
Example: Numbers
  Should Not Be Equal  ${seven_number}  7
  Should Be Equal  ${seven_string}  7
  Should Not Be Equal  ${seven_string}  ${seven_number}
  Should Be Equal As Integers  ${seven_number}  7
  ${seven_number}=  Convert To String  ${seven_number}
  Should Be Equal  ${seven_number}  7

Example: Dictionary - Log Mountain Elevation
  Log Many  &{mountain_elevation}
  Log  ${mountain_elevation}[Brocken]
  Log To Console  ${mountain_elevation}[Zugspitze]

Log Variable types
  ${type}=  Evaluate  type(${mountain_elevation})
  Log To Console  \n
  Log To Console  Variable types
  Log To Console  variable type 'mountain_elevation': {type}
  ${type}=  Evaluate  type(${list})
  Log To Console  variable type 'list': ${type}
  ${type}=  Evaluate  type(${seven_number})
  Log To Console  variable type 'seven_number': ${type}
  ${type}=  Evaluate  type(${seven_string}).__name__
  Log To Console  variable type 'seven_string': ${type}
  Log To Console  \n

Working with lists
  @{list_2}=  Create List  1  2  3  4  5  0  7  42
  ${status}=  Run Keyword And Return Status  Lists Should Be Equal  ${list_2}  ${list}
  ${expected_status}=  Convert To Boolean  ${expected_status}
  Should Be Equal  ${status}  ${expected_status}

Working with dictionaries
  Should Contain  ${mountain_elevation}  Brocken
  Should Not Contain  ${mountain_elevation}  Watzmann
  ${city_population}=  Create Dictionary  Berlin=3.8  Hamburg=1.9
  ${german_facts}  Create List  ${mountain_elevation}  ${city_population}
  ${hamburg_population}  Set Variable  ${german_facts[1]['Hamburg']}
  Should Be Equal  ${hamburg_population}  1.9
