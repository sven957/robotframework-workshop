*** Settings ***
Documentation  Basic example for using Robot Framework standard Telnet library 
...            and using commandline options for ignoring test cases.
Library  Telnet


*** Variables ***
${server}    rainmaker.wunderground.com
${port}      3000


*** Test Cases ***
Example: Connect to weather portal
  Open Connection  ${server}  port=${port}
  Set Prompt  Press Return to continue:
  Write  \r
  Set Prompt  city code--
  Write  \r
  Set Prompt  Selection:
  Write Bare  X\r
  Close Connection