*** Settings ***
Documentation  Example for using an external database library,
...            writing a test suite for an ETL job and using variable files.
Library  DatabaseLibrary
Library  OperatingSystem


*** Test Cases ***
Example
  Connect To Database    dbapiModuleName=psycopg2    dbName=robot    dbUsername=postgres    dbPassword=postgres    dbHost=localhost    dbPort=5432
  Run Keyword And Ignore Error    Execute Sql String    DROP SCHEMA workshop CASCADE
  Execute Sql String   CREATE SCHEMA workshop
  ${res}=  Query  SELECT schema_name FROM information_schema.schemata where schema_name = 'workshop'
  Log  ${res}[0]
  Should Be Equal    workshop    ${res}[0][0]
  Disconnect From Database