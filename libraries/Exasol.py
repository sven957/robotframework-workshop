import pyexasol
from robot.api.deco import keyword

class Exasol():
    """
        Exasol Library for executing queries on Exasol.
        
    """
    def __init__(self, server = 'notSet', user = 'notSet', password = 'notSet', port=8563):
        ROBOT_LIBRARY_SCOPE = 'GLOBAL'
        ROBOT_LIBRARY_DOC_FORMAT = 'HTML'
        self.server = server
        self.port = port
        self.url_str = ''.join([self.server, ":", str(self.port)])
        self.user = user
        self.password = password

    def _getExaConnection():
        self.exa_connection = pyexasol.connect(dsn=self.url_str, user=self.user, password=self.password)

    @keyword('Execute Exasol Query')
    def execute_exasol_query(self, query):
        """
        Execute query on exasol database.

        Examples:
        | Execute Exasol Query | DROP TABLE customers |
        """
        _getExaConnection()
        self.exa_connection.execute(query)

    @keyword('Execute Exasol Query With Result')
    def execute_exasol_query_with_result(self, query):
        """
        Execute query on exasol database.

        Examples:
        | ${res} | Execute Exasol Query With Result | SELECT * FROM customers |
        | Log | ${res}[0][0] |
        """
        _getExaConnection()
        res = self.exa_connection.execute(query)
        return res.fetchall()
